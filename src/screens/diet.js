import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  ImageBackground, Button, Picker} from 'react-native';
import { DemoScrollContent } from '../components/scrollview/entries';
var styles = require('../styles');

export class DietSelectionScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'once'
      };
  }
  

  static navigationOptions = {
    title: 'Schedule Meals'
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
      <Picker
        selectedValue={this.state.regularity}
        style={{height: 50, width: 100}}
        onValueChange={(itemValue, itemIndex) =>
          this.setState({regularity: itemValue})
        }>
        <Picker.Item label="Once" value="once" />
        <Picker.Item label="Daily" value="daily" />
        <Picker.Item label="Weekly" value="weekly" />
        <Picker.Item label="Monthly" value="monthly" />
      </Picker>

      <Picker
        selectedValue={this.state.regularity}
        style={{height: 50, width: 100}}
        onValueChange={(itemValue, itemIndex) =>
          this.setState({regularity: itemValue})
        }>
        <Picker.Item label="Once" value="once" />
        <Picker.Item label="Daily" value="daily" />
        <Picker.Item label="Weekly" value="weekly" />
        <Picker.Item label="Monthly" value="monthly" />
      </Picker>
      
      <Button
        title="Next"
        onPress={() => navigate('Home', {name: 'Jane'})}
      />
      </View>
      
    );
  }
}
