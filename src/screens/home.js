import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  ImageBackground, Button} from 'react-native';
import { DemoScrollContent } from '../components/scrollview/entries';
var styles = require('../styles');

export class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Menu',
    headerStyle: styles.h1
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
      <ScrollView>
        <ImageBackground source={require('../assets/img/scrollview_bg.jpg')} style={{width: '100%', height: '100%'}}>
          <DemoScrollContent navigation={this.props.navigation}/>
        </ImageBackground>
      </ScrollView>
      </View>
      
    );
  }
}
