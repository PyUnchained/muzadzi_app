import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  ImageBackground, Button, Picker} from 'react-native';
import call from 'react-native-phone-call'

import { CheckBox } from 'react-native-elements'
import MapView from 'react-native-maps';

import { DemoScrollContent } from '../components/scrollview/entries';
var styles = require('../styles');

export class MealPlanScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'cc', checked: false
      };
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }
  

  static navigationOptions = {
    title: 'Select Meal Plan'
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <Picker
          selectedValue={this.state.regularity}
          style={{height: 50}}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({regularity: itemValue})
          }>
          <Picker.Item label="Nice Restaurant" value="nr" />
          <Picker.Item label="Best Cafe" value="bc" />
          <Picker.Item label="Super Mart" value="sm" />
          <Picker.Item label="Cajun Cuisine" value="cc" />
        </Picker>
        <ScrollView>
          <ImageBackground source={require('../assets/img/scrollview_bg.jpg')} style={{width: '100%', height: '100%'}}>
            <DemoScrollContent navigation={this.props.navigation}/>
          </ImageBackground>
        </ScrollView>
      </View>
      
    );
  }
}

export class DailySelector extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'daily', checked: false};
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }
  

  render (){
    
    return (<View style={{ flex: 1, flexDirection: 'row'}}>
      <CheckBox
        center
        title='Breakfast'
        size={12}
        checked={this.state.checked}
        onPress={() => this.onChangeCheck()}
      />
      <CheckBox
        center
        title='Lunch'
        size={12}
        checked={this.state.checked}
        onPress={() => this.onChangeCheck()}
      />
      <CheckBox
        center
        title='Dinner'
        size={12}
        checked={this.state.checked}
        onPress={() => this.onChangeCheck()}
      />
    </View>)
  }

}



export class ScheduleMealScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'daily', checked: false
      };
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }
  

  static navigationOptions = {
    title: 'Select Meal Plan'
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <ScrollView>
          <Picker
            selectedValue={this.state.regularity}
            style={{height: 50, width: 200}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({regularity: itemValue})
            }>
            <Picker.Item label="Daily" value="daily" />
            <Picker.Item label="Weekly" value="weekly" />
          </Picker>

          <DailySelector/>
          <View>
            <CheckBox
              center
              title='Mon'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
            <CheckBox
              center
              title='Tue'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
            <CheckBox
              center
              title='Wed'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
            <CheckBox
              center
              title='Thur'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
            <CheckBox
              center
              title='Fri'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
            <CheckBox
              center
              title='Sat'
              checked={this.state.checked}
              onPress={() => this.onChangeCheck()}
            />
          </View>
          <Button
          title = 'Next'
          onPress={() => navigate('ConfirmMeal', {name: 'Jane'})}
          />
        </ScrollView>
      </View>
      
    );
  }
}

export class ConfirmMealScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'daily', checked: false
      };
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }

  onMakePayment(){
    const args = {
      number: '*151*1*1*0782201884*23.4#', 
      prompt: true 
    }
     
    call(args).catch(console.error)
  }
  

  static navigationOptions = {
    title: 'Confirm Meal Plan'
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
      <Text>Total Charge: $23.00</Text>
      <Button
          title = 'Make Payment'
          onPress={this.onMakePayment}
          />
      <Button
          title = 'Next'
          onPress={() => navigate('TrackMeal', {name: 'Jane'})}
          />
      </View>
      
    );
  }
}


export class MealDeliveryTrackingScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { regularity: 'cc', checked: false
      };
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }
  

  static navigationOptions = {
    title: 'Track Delivery'
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column'}}>
      <MapView
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      />
      </View>
      
    );
  }
}