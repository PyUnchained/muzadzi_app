//original profile creen view

<View style={{flex:1, flexDirection:'column', alignItems: 'stretch'}}>
  <TextInput
  style={{height: 40, borderColor: 'gray', borderWidth: 1}}
  placeholder='Full Name'
  />
  <TextInput
  style={{height: 40, borderColor: 'gray', borderWidth: 1}}
  placeholder='Address'
  />
  <Text style={styles.h1}>Dietary Choices</Text>
  <CheckBox
    center
    title='Vegan'
    checked={this.state.checked}
    onPress={() => this.onChangeCheck()}
  />
  <CheckBox
    center
    title='Diabetic'
    checked={this.state.checked}
    onPress={() => this.onChangeCheck()}
  />
  <CheckBox
    center
    title='Low Colesterol'
    checked={this.state.checked}
    onPress={() => this.onChangeCheck()}
  />
  <Button
  title = 'Next'
  onPress={() => navigate('MealSchedule', {name: 'Jane'})}
  />
</View>