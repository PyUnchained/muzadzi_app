import React, {Component} from 'react';
import {Platform, StyleSheet, View, ScrollView, Image,
  ImageBackground, TextInput, Alert, ActivityIndicator, FlatList} from 'react-native';

import { FormLabel, FormInput, FormValidationMessage,
  CheckBox, Button, Icon, Input,  Text} from 'react-native-elements'

import { DemoScrollContent } from '../components/scrollview/entries';
import { SelectMealPlanEntry } from '../components/list_entries';
import Modal from "react-native-modal";

var styles = require('../styles');

export class LoginScreen extends React.Component {
  static navigationOptions = {
    title: 'Muzadzi Login',
    headerStyle: styles.h1
  };

  constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: '',
        cellphone: '',
        email: '',
        isLoading: false,
        isModalVisible: false,
      };
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  doLogin() {
    this.setState({
      isLoading:true
    });
    return fetch('http://173.212.247.232:8031/api/login/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        
      }),

      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({isLoading:false});
        if (responseJson.ok) {
          const {navigate} = this.props.navigation;
          navigate('UserProfile', {username: this.state.username});
        } else {
          Alert.alert('Alert', responseJson.msg);
        }
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({isLoading:false});
        Alert.alert("Alert", "Please check your internet connection and try again.");
      });
  }

  doSignUp() {
    this.setState({
      isLoading:true
    });
    const {navigate} = this.props.navigation;
    return fetch('http://173.212.247.232:8031/api/sign_up/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        cellphone: this.state.cellphone,
        email: this.state.email,
      }),

      })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({isLoading:false});
        this._toggleModal();
        if (responseJson.ok) {
          navigate('UserProfile', {username: this.state.username});
        } else {
          Alert.alert('Alert', responseJson.msg);
        }
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({isLoading:false});
        Alert.alert("Alert", "Please check your internet connection and try again.");
      });
  }


  render() {
    const {navigate} = this.props.navigation;
    if(this.state.isLoading) {
      return(
          <View style={{flex: 1, alignItems: 'center',justifyContent: 'center', padding: 20}}>
            <ActivityIndicator style={{width: 200, height: 200}}/>
          </View>
        )
    }

    else {
      return (
        <View style={{flex:1, flexDirection:'column', alignItems: 'stretch'}}>
          <Image style={{ flex:0.4, width: '100%'}} source={require('../assets/img/logo_lg.png')} />
          <Input
            style={{marginBottom: 20}}
            placeholder='USERNAME'
            onChangeText={(username) => this.setState({username})}
          />
          <View style={{marginBottom: 20}}></View>
          <Input
            style={{marginBottom: 20}}
            placeholder='PASSWORD'
            onChangeText={(password) => this.setState({password})}
          />
          <View style={{marginBottom: 30}}></View>

          <Modal isVisible={this.state.isModalVisible} style={{padding:20}}>
            <View style={{ flex: 1 , backgroundColor:'white', padding:5}}>
              <View style={{marginBottom: 30}}></View>
              <Input
                style={{marginBottom: 20}}
                placeholder='USERNAME'
                onChangeText={(username) => this.setState({username})}
              />
              <View style={{marginBottom: 20}}></View>
              <Input
                style={{marginBottom: 20}}
                placeholder='PASSWORD'
                onChangeText={(password) => this.setState({password})}
              />
              <View style={{marginBottom: 20}}></View>
              <Input
                style={{marginBottom: 20}}
                placeholder='MOBILE'
                onChangeText={(cellphone) => this.setState({cellphone})}
              />
              <View style={{marginBottom: 20}}></View>
              <Input
                style={{marginBottom: 20}}
                placeholder='EMAIL'
                onChangeText={(email) => this.setState({email})}
              />
              <View style={{marginBottom: 40}}></View>
              <View style={{ flex: 0.1 ,flexDirection:'row', justifyContent: 'space-evenly'}}>
                <Button
                title="CANCEL"
                onPress={() => this._toggleModal()}
                />
                <Button
                title="CONFIRM"
                onPress={() => this.doSignUp()}
                />
                
              </View>
              
            </View>
          </Modal>
          
          <View style={{flex:0.3, flexDirection:'row', justifyContent: 'space-evenly', alignItems: 'center'}}>
            <Button
            title="Login"
            onPress={() => this.doLogin()}
            icon={
              <Icon
                raised
                name='chevron-circle-right'
                type='font-awesome'
                size={10}
                color='#56a83c' />
            }
            />
            <Button
            title="Sign Up"
            onPress={() => this._toggleModal()}
            icon={
              <Icon
                raised
                name='key'
                type='font-awesome'
                size={10}
                color='#cc0000' />
            }
            />
          </View>
            
        </View>
      );
    }
    
      
    
  }
}

// 

export class ProfileScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = { checked: false,
      mealPlans: false,
      userProfile: false,
      current_plan: false};
  }

  componentDidMount() {
    
    fetch("http://173.212.247.232:8031/tastypie/v1/mealplan/")
      .then(res => res.json())
      .then(
        (result) => {
          
          this.setState({
            mealPlans: result.objects
          });
        },
        (error) => {
          console.log(error);
        }
      )

    const { navigation } = this.props;
    const username = navigation.getParam('username', 'User');
    var profile_url = 'http://173.212.247.232:8031/tastypie/v1/customerprofile/?user__username='+username;

    fetch(profile_url)
      .then(res => res.json())
      .then(
        (result) => {
          
          this.setState({
            userProfile: result.objects
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
        }
      )
  }

  static navigationOptions = {
    title: 'Muzadzi Home',
    headerStyle: styles.h1
  };

  onChangeCheck() {
    this.setState({ checked: !this.state.checked})
  }


  render() {
    const {navigate} = this.props.navigation;

    // Make sure to only attempt to render the component fully if the request to retrieve
    // the user's profile information succeeds
    if (this.state.mealPlans && this.state.userProfile) {
      const profile_info = this.state.userProfile[0];
      var current_meal_plan = 'None';
      if (profile_info.current_meal_plan) {
        var current_meal_plan = profile_info.current_meal_plan;
      }
      const { navigation } = this.props;
      const username = navigation.getParam('username', 'User');

      return (
        <View style={{flex:1, flexDirection:'column', alignItems: 'stretch'}}>
          <View style={{
            flex: 0.2,
            flexDirection: 'column',
            justifyContent: 'space-evenly',
          }}>
          <Text h3>Welcome {username}</Text>
          <Text>Current Meal Plan: {current_meal_plan}</Text>
          </View>
          <View style={{
            flex: 0.8,
            flexDirection: 'row',
            justifyContent: 'flex-start',
          }}>
            <FlatList
              data={this.state.mealPlans}
              renderItem={({item}) => <SelectMealPlanEntry meal_plans={this.state.mealPlans} profile_info={profile_info} object={item} navigation={this.props.navigation}/>}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator style={{width: 200, height: 200}}/>
        </View>
      );

    }
    
  }
}