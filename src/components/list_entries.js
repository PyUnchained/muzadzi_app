import React, {Component} from 'react';
import {View, Image, StyleSheet, TouchableOpacity, Button} from 'react-native';
import { Icon, Text } from 'react-native-elements'
import Modal from "react-native-modal";
import call from 'react-native-phone-call'

import { SimpleForm } from '../forms/components';
import { BooleanField } from '../forms/fields';

var styles = require('../styles');


export class SelectMealPlanEntry extends Component {

    state = {
      isModalVisible: false,
    };

    _toggleModal = () =>
      this.setState({ isModalVisible: !this.state.isModalVisible });


    ecocashPayment(object, profile_info){

        var meal_plan_uri = object.resource_uri;
        var profile_uri = profile_info.resource_uri;
        

        // Turn all the items the user has added into an Array
        var menuItems = []
        for(var key in this.newMealPlanForm.state) {
          var value = this.newMealPlanForm.state[key];
          if (key.includes('_is_checked'))  {
            if (value == true) {
              menuItems.push(key.replace('_is_checked',''));
            }
          }
         
        }

        // Turn the days the user has selected for their schedule into a list
        var schedule = []
        var day_list = ['mon','tue','wed','thur','fri','sat','sun']
        for(var key in this.newMealPlanForm.deliveryRegularityForm.state) {
          var value = this.newMealPlanForm.deliveryRegularityForm.state[key];
          
          for (var day_index in day_list){
            if (key == day_list[day_index])  {
              if (value == true) {
                schedule.push(key);
              }
            }
          }
          
         
        }

        // console.log('Payment JSON:\n');
        // console.log(schedule);
        // console.log(this.newMealPlanForm.state.sum_tot);
        // console.log(profile_info.user);
        // console.log(menuItems);
        // console.log(meal_plan_uri);
        // console.log(this.newMealPlanForm.deliveryRegularityForm.state.daily)

        // fetch('http://173.212.247.232:8031/tastypie/v1/order/', {
        //   method: 'POST',

        //   headers: {
        //     Accept: 'application/json',
        //     'Content-Type': 'application/json',
        //   },

        //   body: JSON.stringify({
        //     total: this.newMealPlanForm.state.sum_tot,
        //     user: profile_info.user,
        //     menu_items: menuItems,
        //     meal_plan: meal_plan_uri,
        //     daily_delivery: this.newMealPlanForm.deliveryRegularityForm.state.daily,
        //     schedule:schedule

        //   }),

        // })
        // .then((response) => response.json())
        // .then((responseJson) => {
        //   if (responseJson.id) {
        //     console.log(responseJson);
        //     console.log('Found ID in Json');
        //   } else {
        //     Alert.alert('Alert', responseJson.error);
        //   }
        // })
        // .catch((error) => {
        //   Alert.alert("Alert", "Please check your internet connection and try again.");
        // });

        // fetch("http://173.212.247.232:8031/tastypie/v1/mealplan/")
        //   .then(res => res.json())
        //   .then(
        //     (result) => {
              
        //       this.setState({
        //         mealPlans: result.objects
        //       });
        //     },
        //     (error) => {
        //       console.log(error);
        //     }
        //   )

        const args = {
          number: '*151*2*2*45556*' + object.amount + '#', // String value with the number to call
          prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
         
        // call(args).catch(console.error)
    }

    getFormFields(component) {
      var form_fields = component.props.object.menu_items.map(function(menu_item, index){
        return new BooleanField(menu_item.description,
          menu_item);
      });
      return form_fields;
    }



    render() {
        const {navigate} = this.props.navigation;
        var image_url = 'http://173.212.247.232:8031' + this.props.object.image;
        
        return (
            <View style={{marginBottom:30,paddingLeft:10,paddingRight:10, flex:1, flexDirection:'row'}}>
                
                <View style ={{flex:1, flexDirection:'column',paddingRight:5}}>
                    
                    <Image
                      style={{width: '100%', height: 100}}
                      source={{uri: image_url}}
                    />
                    <Button
                    title="order"
                    style={{width: 150}}
                    onPress={() => this._toggleModal()}
                    />

                    <View style={{ flex: 1 }}>
                      <Modal isVisible={this.state.isModalVisible} style={{padding:0}}>
                        <View style={{ flex: 1 , backgroundColor:'white', padding:5}}>
                          <Text h1 style={{textAlign: 'center', color:'#cc0000'}}>{this.props.object.title}</Text>
                          <SimpleForm fields={this.getFormFields(this)} ref={component => this.newMealPlanForm = component}/>
                          <View style={{ flex: 0.1 ,flexDirection:'row', justifyContent: 'space-evenly'}}>
                            <Button
                            title="Cancel"
                            onPress={() => this._toggleModal()}
                            />
                            <Button
                            title="Purchase"
                            onPress={() => this.ecocashPayment(this.props.object, this.props.profile_info)}
                            />
                          </View>
                          
                        </View>
                      </Modal>
                    </View>

                </View>


                <View style ={{flex:1, flexDirection:'column'}}>
                <Text h3 style={{color:'#cc0000'}}>{this.props.object.title}</Text>
                <Text style={{ fontSize:16, fontWeight: 'bold'}}>{this.props.object.available_time}</Text>
                </View>
                
            </View>
            
        
        );
        }
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1, flexDirection: 'row',
    margin: 10, backgroundColor: 'rgba(52, 52, 52, 0.4)',
    padding: 5,borderRadius:2,alignItems: 'center'
  },
  scrollViewImg: {
    flex: 1, height: 150
  },
  scrollViewDetailContainer: {
    flex: 1, flexDirection: 'column',
    marginLeft: 5, 
  },
  scrollViewIconContainer: {
    flex: 1, flexDirection: 'row',
    justifyContent: 'center'
  },
  scrollViewText: {
    flex: 1,textAlign: 'center',

  },
  scrollViewPrice: {
    fontSize: 25, flex: 1,
    textAlign: 'center',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
});