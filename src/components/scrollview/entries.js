import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, Button} from 'react-native';
import { Icon } from 'react-native-elements'

const entryText = 'This is a really tasty treat full of so much goodness... '

export class DemoScrollContent extends Component {

render() {
	const {navigate} = this.props.navigation;
	return (
	<View>
		<View style={styles.scrollViewContainer}>
			<Image style={styles.scrollViewImg} source={require('../../assets/img/demo_food1.jpg')} />
			<View style={styles.scrollViewDetailContainer}>
				<Text style={styles.scrollViewText}>{entryText}</Text>
				
				<View style={styles.scrollViewIconContainer}>
					<Icon
					  raised
					  size={12}
					  name='heartbeat'
					  type='font-awesome'
					  color='#f9725c'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='shopping-bag'
					  type='font-awesome'
					  color='#31cf00'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='tags'
					  type='font-awesome'
					  color='#b8c6b3'
					  onPress={() => console.log('hello')} />
				</View>
				<Text style={styles.scrollViewPrice}>{'$15.50'}</Text>
				<Button
				  title="Add"
				  onPress={() => navigate('CreateMealSchedule', {name: 'Jane'})}
				/>
			</View>
		</View>

		<View style={styles.scrollViewContainer}>
			<Image style={styles.scrollViewImg} source={require('../../assets/img/demo_food2.jpg')} />
			<View style={styles.scrollViewDetailContainer}>
				<Text style={styles.scrollViewText}>{entryText}</Text>
				
				<View style={styles.scrollViewIconContainer}>
					<Icon
					  raised
					  size={12}
					  name='heartbeat'
					  type='font-awesome'
					  color='#f9725c'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='shopping-bag'
					  type='font-awesome'
					  color='#31cf00'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='tags'
					  type='font-awesome'
					  color='#b8c6b3'
					  onPress={() => console.log('hello')} />
				</View>
				<Text style={styles.scrollViewPrice}>{'$8.76'}</Text>
			</View>
		</View>

		<View style={styles.scrollViewContainer}>
			<Image style={styles.scrollViewImg} source={require('../../assets/img/demo_food3.jpg')} />
			<View style={styles.scrollViewDetailContainer}>
				<Text style={styles.scrollViewText}>{entryText}</Text>
				
				<View style={styles.scrollViewIconContainer}>
					<Icon
					  raised
					  size={12}
					  name='heartbeat'
					  type='font-awesome'
					  color='#f9725c'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='shopping-bag'
					  type='font-awesome'
					  color='#31cf00'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='tags'
					  type='font-awesome'
					  color='#b8c6b3'
					  onPress={() => console.log('hello')} />
				</View>
				<Text style={styles.scrollViewPrice}>{'$4.55'}</Text>
			</View>
		</View>

		<View style={styles.scrollViewContainer}>
			<Image style={styles.scrollViewImg} source={require('../../assets/img/demo_food4.jpg')} />
			<View style={styles.scrollViewDetailContainer}>
				<Text style={styles.scrollViewText}>{entryText}</Text>
				
				<View style={styles.scrollViewIconContainer}>
					<Icon
					  raised
					  size={12}
					  name='heartbeat'
					  type='font-awesome'
					  color='#f9725c'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='shopping-bag'
					  type='font-awesome'
					  color='#31cf00'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='tags'
					  type='font-awesome'
					  color='#b8c6b3'
					  onPress={() => console.log('hello')} />
				</View>
				<Text style={styles.scrollViewPrice}>{'$4.50'}</Text>
			</View>
		</View>

		<View style={styles.scrollViewContainer}>
			<Image style={styles.scrollViewImg} source={require('../../assets/img/demo_food5.jpg')} />
			<View style={styles.scrollViewDetailContainer}>
				<Text style={styles.scrollViewText}>{entryText}</Text>
				
				<View style={styles.scrollViewIconContainer}>
					<Icon
					  raised
					  size={12}
					  name='heartbeat'
					  type='font-awesome'
					  color='#f9725c'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='shopping-bag'
					  type='font-awesome'
					  color='#31cf00'
					  onPress={() => console.log('hello')} />
					<Icon
					  raised
					  size={12}
					  name='tags'
					  type='font-awesome'
					  color='#b8c6b3'
					  onPress={() => console.log('hello')} />
				</View>
				<Text style={styles.scrollViewPrice}>{'$2.50'}</Text>
			</View>
		</View>
	</View>
	
	
	);
	}
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1, flexDirection: 'row',
    margin: 10, backgroundColor: 'rgba(52, 52, 52, 0.4)',
    padding: 5,borderRadius:2,alignItems: 'center'
  },
  scrollViewImg: {
  	flex: 1, height: 150
  },
  scrollViewDetailContainer: {
  	flex: 1, flexDirection: 'column',
  	marginLeft: 5, 
  },
  scrollViewIconContainer: {
  	flex: 1, flexDirection: 'row',
  	justifyContent: 'center'
  },
  scrollViewText: {
  	flex: 1,textAlign: 'center',

  },
  scrollViewPrice: {
  	fontSize: 25, flex: 1,
  	textAlign: 'center',
  	fontWeight: 'bold',
  	textDecorationLine: 'underline'
  },
});