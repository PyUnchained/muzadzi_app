import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  ImageBackground, Button, Picker} from 'react-native';
import { CheckBox } from 'react-native-elements';

var styles = require('../styles');



export class SimpleField extends React.Component {

  constructor(props){
    super(props);
    this.name = props.name;
  }

  render() {
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <Text>My Form is Here</Text>
      </View>
      
    );
  }
}

export class Field {
  constructor(name, obj) {
    this.name = name;
    this.object = obj; //This is redundant and should be renamed 'obj' soon
    this.obj = obj;
    this.idSuffix = null;
  }

  getID () {
    return this.obj.resource_uri + this.idSuffix;
  }

  render() {
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <Text>Field</Text>
      </View>
      
    );
  }

}

export class BooleanField extends Field {

  constructor(name, obj) {
    super(name, obj)
    this.type = 'BooleanField';
    this.idSuffix = '_is_checked';
  }

  render() {
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <Text>Field</Text>
      </View>
      
    );
  }

}

