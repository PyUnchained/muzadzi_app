import React, {Component} from 'react';
import {Platform, StyleSheet, View, ScrollView, Image,
  ImageBackground, Picker} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage,
    CheckBox, Button, Icon, Input,  Text} from 'react-native-elements'
import MapView from 'react-native-maps';

var styles = require('../styles');

class DeliveryRegularityForm extends React.Component {

  constructor(props){
    super(props);
    this.state = {daily:true, mon:false, tue:false, wed:false, thur:false,
      fri:false, sat:false, sun:false}
  }

  onChangeCheck(){
    this.setState({ daily: !this.state.daily });
  }

  onChangeWeeklyCheck(day){
    this.setState({ [day]: !this.state[day] });
  }

  renderCalendar(component){
    if (component.state.daily) {

      return

    } else {

      return (
        <View style={{flex:1, flexDirection:'column'}}>
          <View style={{flex:0.5, flexDirection:'row'}}>
            <CheckBox
              center
              title='Mon'
              size={12}
              checked={component.state.mon}
              onPress={() => component.onChangeWeeklyCheck('mon')}
            />
            <CheckBox
              center
              title='Tue'
              size={12}
              checked={component.state.tue}
              onPress={() => component.onChangeWeeklyCheck('tue')}
            />
            <CheckBox
              center
              title='Wed'
              size={12}
              checked={component.state.wed}
              onPress={() => component.onChangeWeeklyCheck('wed')}
            />
            

          </View>
          <View style={{flex:0.5, flexDirection:'row'}}>
            <CheckBox
              center
              title='Thur'
              size={12}
              checked={component.state.thur}
              onPress={() => component.onChangeWeeklyCheck('thur')}
            />
            <CheckBox
              center
              title='Fri'
              size={12}
              checked={component.state.fri}
              onPress={() => component.onChangeWeeklyCheck('fri')}
            />
            <CheckBox
              center
              title='Sat'
              size={12}
              checked={component.state.sat}
              onPress={() => component.onChangeWeeklyCheck('sat')}
            />
          </View>
        </View>
      );
    }

  }

  render() {
    
    var calendar_picker = this.renderCalendar(this);
    return (
      <View style={{flex:0.5, flexDirection:'column'}}>
        <View style={{flex:0.4, flexDirection:'row'}}>
          <CheckBox
            center
            title='Daily'
            size={12}
            checked={this.state.daily}
            onPress={() => this.onChangeCheck()}
          />
          <CheckBox
            center
            title='Weekly'
            size={12}
            checked={!this.state.daily}
            onPress={() => this.onChangeCheck()}
          />

          

        </View>

        <View style={{flex:0.6, flexDirection:'row'}}>
          {calendar_picker}
        </View>
      </View>
      
    );
  }

}
export class SimpleForm extends React.Component {

  constructor(props){
    super(props);
    this.fields = props.fields;
    this.state = {sum_tot:0};

    // Set the initial state for all checkboxes to unchecked
    var i;
    for (i = 0; i < this.fields.length; i++) {
      var field = this.fields[i];
      var property_name = this.getIsCheckedStateName(field);
      this.state[property_name] = false;
    }
    // this.onChangeCheck = this.onChangeCheck.bind(this);
    this.renderFields = this.renderFields.bind(this);
    // this.getIsCheckedStateName = this.getIsCheckedStateName.bind(this);
  }

  componentDidMount() {
    fetch("http://173.212.247.232:8031/tastypie/v1/mealplan/")
      .then(res => res.json())
      .then(
        (result) => {
          
          this.setState({
            mealPlans: result.objects
          });
        },
        (error) => {
          console.log(error);
        }
      )
  }

  getIsCheckedStateName(field){
    return field.getID()
  }

  onChangeCheck(checked_state_ref, obj){
    this.setState({ [checked_state_ref]: !this.state[checked_state_ref]});
    if (!this.state[checked_state_ref]) {
      this.setState({sum_tot: this.state.sum_tot + parseFloat(obj.price)})
    } else {
      this.setState({sum_tot: this.state.sum_tot - parseFloat(obj.price)})
    }
  }

  renderFields(component) {

    rendered_fields = this.fields.map( function (field, index) {
      var field_key = field.name + '-' + index;
      var checked_state_ref = field.getID();
      var onChangeCheck = this.onChangeCheck;
      return (
        
        <CheckBox
          center
          title={field.name}
          size={12}
          key={field_key}
          checked={component.state[checked_state_ref]}
          onPress={() => component.onChangeCheck(checked_state_ref, field.object)}
        />
      );
      
    });

    if (rendered_fields.length == 0) {
      return (
        <Text style={{textAlign: 'center'}}>There are currently no menu items associated with this meal plan.</Text>
        )
    } else {
      return rendered_fields
    }
    
    
  }

  render() {
    
    var form_fields = this.renderFields(this);
    return (
      <View style={{flex:1, flexDirection:'column'}}>
        <DeliveryRegularityForm parent={this} ref={component => this.deliveryRegularityForm = component}/>
        <Text h4 style={{textAlign: 'center'}}>Menu Items</Text>
        <Text style={{textAlign: 'center'}}>Total: ${this.state.sum_tot}</Text>
        {form_fields}
      </View>
      
    );
  }
}
