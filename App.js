/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  ImageBackground, Button} from 'react-native';

import {createStackNavigator, createAppContainer} from 'react-navigation';

import { DemoScrollContent } from './src/components/scrollview/entries';
import { HomeScreen } from './src/screens/home';
import { MealPlanScreen, ScheduleMealScreen, ConfirmMealScreen, MealDeliveryTrackingScreen } from './src/screens/meals';
import { DietSelectionScreen } from './src/screens/diet';
import { LoginScreen, ProfileScreen } from './src/screens/auth';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});



const MainNavigator = createStackNavigator({
  Login: {screen: LoginScreen},
  UserProfile: {screen: ProfileScreen},
  DietSelection: {screen: DietSelectionScreen},
  Home: {screen: HomeScreen},
  MealSchedule : {screen: MealPlanScreen},
  CreateMealSchedule: {screen: ScheduleMealScreen},
  ConfirmMeal : {screen: ConfirmMealScreen},
  TrackMeal: {screen: MealDeliveryTrackingScreen}
});

const App = createAppContainer(MainNavigator);
export default App;

// type Props = {};
// export default class App extends Component<Props> {
//   render() {
//     return (
      
      // <View style={{flex:1, flexDirection:'column'}}>
      // <Text style={styles.h1}>{'Muzadzi Menu 2'}</Text>
      // <ScrollView>
      //   <ImageBackground source={require('./src/assets/img/scrollview_bg.jpg')} style={{width: '100%', height: '100%'}}>
      //     <DemoScrollContent/>
      //   </ImageBackground>
      // </ScrollView>

      // </View>
      
//     );
//   }
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  h1: {
    fontSize: 35,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

  scrollViewContainer: {
    flex: 1, flexDirection: 'row'
  }
});
